.. NCQRS documentation master file, created by
   sphinx-quickstart on Thu Oct 20 15:31:13 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. raw:: html

    <style> 
    .dont { color:red; font-weight: bold; }
    .do { color: green; font-weight: bold; }
    .consider { color: blue; font-weight: bold; }
    .avoid { color: orange; font-weight: bold; }
    </style>

.. role:: dont
.. role:: do
.. role:: consider
.. role:: avoid

Welcome to Cognitive X Solutions Inc C# Style Guide!
=================================

Welcome to the Cognitive X Solutions C# Style Guide.  The follow guide will outline to you how Cognitive X writes C# code.  
The guidelines are laid out in a simple format prefixed with the terms :do:`DO`, 
:consider:`CONSIDER`, :avoid:`AVOID`, :dont:`DON’T`. There are times when 
these guidelines might need to be violated, but there should be a clear and compelling reason to do so (and should be 
discussed with team before doing so).

This guide is based on https://msdn.microsoft.com/en-us/library/ms229042.aspx

.. toctree::
   :maxdepth: 3

   namingconventions
   typedesign
   memberdesign