.. raw:: html

    <style> 
    .dont { color:red; font-weight: bold; }
    .dont:before {
        content: 'X ';
    }
    .do { color: green; font-weight: bold; }
    .do::before, .consider:before {
        content: '✓ ';
    }
    .consider { color: blue; font-weight: bold; }
    .avoid { color: orange; font-weight: bold; }
    td strong { color: green; }
    .monospace { font-family: Courier; }
    </style>

.. role:: dont
.. role:: do
.. role:: consider
.. role:: avoid
.. role:: monospace
.. |br| raw:: html

   <br />
    
Member Design Guidelines
============

Methods, properties, events, constructors, and fields are collectively referred to as members. 
Members are ultimately the means by which framework functionality is exposed to the end users of 
a framework.

Members can be virtual or nonvirtual, concrete or abstract, static or instance, and can have 
several different scopes of accessibility. All this variety provides incredible expressiveness 
but at the same time requires care on the part of the framework designer.

This chapter offers basic guidelines that should be followed when designing members of any type.

Member Overloading
------------------

Member overloading means creating two or more members on the same type that differ only in the 
number or type of parameters but have the same name. For example, in the following, the WriteLine 
method is overloaded::

    public static class Console {
        public void WriteLine();
        public void WriteLine(string value);
        public void WriteLine(bool value);
        ...
    }

Because only methods, constructors, and indexed properties can have parameters, only those members can be overloaded.

Overloading is one of the most important techniques for improving usability, productivity, and 
readability of reusable libraries. Overloading on the number of parameters makes it possible to 
provide simpler versions of constructors and methods. Overloading on the parameter type makes 
it possible to use the same member name for members performing identical operations on a selected 
set of different types.

:do:`DO` try to use descriptive parameter names to indicate the default used by shorter overloads.

:avoid:`AVOID` arbitrarily varying parameter names in overloads. If a parameter in one overload 
represents the same input as a parameter in another overload, the parameters should have the same name.

:avoid:`AVOID` being inconsistent in the ordering of parameters in overloaded members. Parameters 
with the same name should appear in the same position in all overloads.

:do:`DO` make only the longest overload virtual (if extensibility is required). Shorter overloads 
should simply call through to a longer overload.

:dont:`DO NOT` use ref or out modifiers to overload members.

Some languages cannot resolve calls to overloads like this. In addition, such overloads usually have 
completely different semantics and probably should not be overloads but two separate methods instead.

:dont:`DO NOT` have overloads with parameters at the same position and similar types yet with 
different semantics.

:do:`DO` allow null to be passed for optional arguments.

:do:`DO` use member overloading rather than defining members with default arguments when you are 
creating libraries that will be used outside the company (or from a different language, as default 
arguments are not CLS compliant).

Property Design
---------------

Although properties are technically very similar to methods, they are quite different in terms of 
their usage scenarios. They should be seen as smart fields. They have the calling syntax of fields, 
and the flexibility of methods.

:do:`DO` create get-only properties if the caller should not be able to change the value of the property.

Keep in mind that if the type of the property is a mutable reference type, the property value can be 
changed even if the property is get-only.

:dont:`DO NOT` provide set-only properties or properties with the setter having broader accessibility than 
the getter.

For example, do not use properties with a public setter and a protected getter.

If the property getter cannot be provided, implement the functionality as a method instead. Consider 
starting the method name with Set and follow with what you would have named the property. For example, 
AppDomain has a method called SetCachePath instead of having a set-only property called CachePath.

:do:`DO` provide sensible default values for all properties, ensuring that the defaults do not result in 
a security hole or terribly inefficient code.

:do:`DO` allow properties to be set in any order even if this results in a temporary invalid state of the 
object.

It is common for two or more properties to be interrelated to a point where some values of one 
property might be invalid given the values of other properties on the same object. In such cases, 
exceptions resulting from the invalid state should be postponed until the interrelated properties are 
actually used together by the object.

:do:`DO` preserve the previous value if a property setter throws an exception.  Which means that you should
check the validity of a value before storing the new value to the underlying field.

:avoid:`AVOID` throwing exceptions from property getters.

Property getters should be simple operations and should not have any preconditions. If a getter can 
throw an exception, it should probably be redesigned to be a method. Notice that this rule does not 
apply to indexers, where we do expect exceptions as a result of validating the arguments.

Indexed Property Design
^^^^^^^^^^^^^^^^^^^^^^^

An indexed property is a special property that can have parameters and can be called with special 
syntax similar to array indexing.

Indexed properties are commonly referred to as indexers. Indexers should be used only in APIs that 
provide access to items in a logical collection. For example, a string is a collection of characters, 
and the indexer on System.String was added to access its characters.

:consider:`CONSIDER` using indexers to provide access to data stored in an internal array.

:consider:`CONSIDER` providing indexers on types representing collections of items.

:avoid:`AVOID` using indexed properties with more than one parameter.

If the design requires multiple parameters, reconsider whether the property really represents an 
accessor to a logical collection. If it does not, use methods instead. Consider starting the method 
name with Get or Set.

:avoid:`AVOID` indexers with parameter types other than System.Int32, System.Int64, System.String, 
System.Object, or an enum.

If the design requires other types of parameters, strongly reevaluate whether the API really 
represents an accessor to a logical collection. If it does not, use a method. Consider starting the 
method name with Get or Set.

:do:`DO` use the name Item for indexed properties unless there is an obviously better name (e.g., see the 
Chars property on System.String).

In C#, indexers are by default named Item. The IndexerNameAttribute can be used to customize this name.

:dont:`DO NOT` provide both an indexer and methods that are semantically equivalent.

:dont:`DO NOT` provide more than one family of overloaded indexers in one type.

This is enforced by the C# compiler.

:dont:`DO NOT` use nondefault indexed properties.

This is enforced by the C# compiler.

Property Change Notification Events
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Sometimes it is useful to provide an event notifying the user of changes in a property value. 
For example, System.Windows.Forms.Control raises a TextChanged event after the value of its Text 
property has changed.

:consider:`CONSIDER` raising change notification events when property values in high-level APIs (usually 
designer components) are modified.

If there is a good scenario for a user to know when a property of an object is changing, the object 
should raise a change notification event for the property.

However, it is unlikely to be worth the overhead to raise such events for low-level APIs such as base 
types or collections. For example, List<T> would not raise such events when a new item is added to the 
list and the Count property changes.

:consider:`CONSIDER` raising change notification events when the value of a property changes via external forces.

If a property value changes via some external force (in a way other than by calling methods on the 
object), raise events indicate to the developer that the value is changing and has changed. A good 
example is the Text property of a text box control. When the user types text in a TextBox, the 
property value automatically changes.

Constructor Design
------------------

There are two kinds of constructors: type constructors and instance constructors.

Type constructors are static and are run by the CLR before the type is used. Instance constructors 
run when an instance of a type is created.

Type constructors cannot take any parameters. Instance constructors can. Instance constructors that 
don’t take any parameters are often called default constructors.

Constructors are the most natural way to create instances of a type. Most developers will search and 
try to use a constructor before they consider alternative ways of creating instances (such as factory 
methods).

:consider:`CONSIDER` providing simple, ideally default, constructors.

A simple constructor has a very small number of parameters, and all parameters are primitives or enums. 
Such simple constructors increase usability of the framework.

:consider:`CONSIDER` using a static factory method instead of a constructor if the semantics of the desired 
operation do not map directly to the construction of a new instance, or if following the constructor 
design guidelines feels unnatural.

:do:`DO` use constructor parameters as shortcuts for setting main properties.

There should be no difference in semantics between using the empty constructor followed by some 
property sets and using a constructor with multiple arguments.

:do:`DO` use the same name for constructor parameters and a property if the constructor parameters are 
used to simply set the property.

The only difference between such parameters and the properties should be casing.

:do:`DO` minimal work in the constructor.

Constructors should not do much work other than capture the constructor parameters. The cost of any 
other processing should be delayed until required.

:do:`DO` throw exceptions from instance constructors, if appropriate.

:do:`DO` explicitly declare the public default constructor in classes, if such a constructor is required.

If you don’t explicitly declare any constructors on a type, many languages (such as C#) will 
automatically add a public default constructor. (Abstract classes get a protected constructor.)

Adding a parameterized constructor to a class prevents the compiler from adding the default 
constructor. This often causes accidental breaking changes.

:avoid:`AVOID` explicitly defining default constructors on structs.

This makes array creation faster, because if the default constructor is not defined, it does not have 
to be run on every slot in the array. Note that many compilers, including C#, don’t allow structs to 
have parameterless constructors for this reason.

:avoid:`AVOID` calling virtual members on an object inside its constructor.

Calling a virtual member will cause the most derived override to be called, even if the constructor of 
the most derived type has not been fully run yet.

Type Constructor Guidelines
^^^^^^^^^^^^^^^^^^^^^^^^^^^

:do:`DO` make static constructors private.

A static constructor, also called a class constructor, is used to initialize a type. The CLR calls the 
static constructor before the first instance of the type is created or any static members on that type 
are called. The user has no control over when the static constructor is called. If a static constructor 
is not private, it can be called by code other than the CLR. Depending on the operations performed in 
the constructor, this can cause unexpected behavior. The C# compiler forces static constructors to be 
private.

:dont:`DO NOT` throw exceptions from static constructors.

If an exception is thrown from a type constructor, the type is not usable in the current application 
domain.

:consider:`CONSIDER` initializing static fields inline rather than explicitly using static constructors, because 
the runtime is able to optimize the performance of types that don’t have an explicitly defined static 
constructor.

Event Design
------------

Events are the most commonly used form of callbacks (constructs that allow the framework to call into 
user code). Other callback mechanisms include members taking delegates, virtual members, and 
interface-based plug-ins. Data from usability studies indicate that the majority of developers are 
more comfortable using events than they are using the other callback mechanisms. Events are nicely 
integrated with Visual Studio and many languages.

It is important to note that there are two groups of events: events raised before a state of the system 
changes, called pre-events, and events raised after a state changes, called post-events. An example of 
a pre-event would be Form.Closing, which is raised before a form is closed. An example of a post-event 
would be Form.Closed, which is raised after a form is closed.

:do:`DO` use the term "raise" for events rather than "fire" or "trigger."

:do:`DO` use System.EventHandler<TEventArgs> instead of manually creating new delegates to be used as event 
handlers.

:consider:`CONSIDER` using a subclass of EventArgs as the event argument, unless you are absolutely sure the 
event will never need to carry any data to the event handling method, in which case you can use the 
EventArgs type directly.

If you ship an API using EventArgs directly, you will never be able to add any data to be carried with 
the event without breaking compatibility (the 'O' in SOLID). If you use a subclass, even if initially 
completely empty, you will be able to add properties to the subclass when needed.

:do:`DO` use a protected virtual method to raise each event. This is only applicable to nonstatic events on 
unsealed classes, not to structs, sealed classes, or static events.

The purpose of the method is to provide a way for a derived class to handle the event using an override. 
Overriding is a more flexible, faster, and more natural way to handle base class events in derived 
classes. By convention, the name of the method should start with "On" and be followed with the name of 
the event.

The derived class can choose not to call the base implementation of the method in its override. Be 
prepared for this by not including any processing in the method that is required for the base class to 
work correctly.

:do:`DO` take one parameter to the protected method that raises an event.

The parameter should be named e and should be typed as the event argument class.

:dont:`DO NOT` pass null as the sender when raising a nonstatic event.

:do:`DO` pass null as the sender when raising a static event.

:dont:`DO NOT` pass null as the event data parameter when raising an event.

You should pass EventArgs.Empty if you don’t want to pass any data to the event handling method. 
Developers expect this parameter not to be null.

:consider:`CONSIDER` raising events that the end user can cancel. This only applies to pre-events.

Use System.ComponentModel.CancelEventArgs or its subclass as the event argument to allow the end user 
to cancel events.

Custom Event Handler Design
^^^^^^^^^^^^^^^^^^^^^^^^^^^

There are cases in which EventHandler<T> cannot be used, such as when the framework needs to work with 
earlier versions of the CLR, which did not support Generics. In such cases, you might need to design 
and develop a custom event handler delegate.

:do:`DO` use a return type of void for event handlers.

An event handler can invoke multiple event handling methods, possibly on multiple objects. If event 
handling methods were allowed to return a value, there would be multiple return values for each 
event invocation.

:do:`DO` use object as the type of the first parameter of the event handler, and call it sender.

:do:`DO` use System.EventArgs or its subclass as the type of the second parameter of the event handler, and call it e.

:dont:`DO NOT` have more than two parameters on event handlers.

Field Design
------------

The principle of encapsulation is one of the most important notions in object-oriented design. This 
principle states that data stored inside an object should be accessible only to that object.

A useful way to interpret the principle is to say that a type should be designed so that changes to 
fields of that type (name or type changes) can be made without breaking code other than for members of 
the type. This interpretation immediately implies that all fields must be private.  This is to help
enforce the 'O' in SOLID.

We exclude constant and static read-only fields from this strict restriction, because such fields, 
almost by definition, are never required to change.

:dont:`DO NOT` provide instance fields that are public or protected.

You should provide properties for accessing fields instead of making them public or protected.  If you
feel you need to violate this guideline, it might indicate that you should consider using a struct
rather than a class.

:do:`DO` use constant fields for constants that will never change.

The compiler burns the values of const fields directly into calling code. Therefore, const values can 
never be changed without the risk of breaking compatibility.

:do:`DO` use public static readonly fields for predefined object instances.

If there are predefined instances of the type, declare them as public read-only static fields of the 
type itself.

:dont:`DO NOT` assign instances of mutable types to readonly fields.

A mutable type is a type with instances that can be modified after they are instantiated. For example, 
arrays, most collections, and streams are mutable types, but System.Int32, System.Uri, and 
System.String are all immutable. The read-only modifier on a reference type field prevents the 
instance stored in the field from being replaced, but it does not prevent the field’s instance data 
from being modified by calling members changing the instance.  If you need to enforce read-only on
an object / complex type, you should look into PostSharp's Immutable aspect.

Extension Methods
-----------------

Extension methods are a language feature that allows static methods to be called using instance method 
call syntax. These methods must take at least one parameter, which represents the instance the method 
is to operate on.

The class that defines such extension methods is referred to as the "sponsor" class, and it must be 
declared as static. To use extension methods, one must import the namespace defining the sponsor class.

:avoid:`AVOID` frivolously defining extension methods, especially on types you don’t own.

If you do own source code of a type, consider using regular instance methods instead. If you don’t own, 
and you want to add a method, be very careful. Liberal use of extension methods has the potential of 
cluttering APIs of types that were not designed to have these methods.

:consider:`CONSIDER` using extension methods in any of the following scenarios:

- To provide helper functionality relevant to every implementation of an interface, if said 
functionality can be written in terms of the core interface. This is because concrete implementations 
cannot otherwise be assigned to interfaces. For example, the LINQ to Objects operators are implemented 
as extension methods for all IEnumerable<T> types. Thus, any IEnumerable<> implementation is 
automatically LINQ-enabled.

- When an instance method would introduce a dependency on some type, but such a dependency would 
break dependency management rules. For example, a dependency from String to System.Uri is probably 
not desirable, and so String.ToUri() instance method returning System.Uri would be the wrong design 
from a dependency management perspective. A static extension method Uri.ToUri(this string str) 
returning System.Uri would be a much better design.

:avoid:`AVOID` defining extension methods on System.Object.

VB users will not be able to call such methods on object references using the extension method syntax. 
VB does not support calling such methods because, in VB, declaring a reference as Object forces all 
method invocations on it to be late bound (actual member called is determined at runtime), while 
bindings to extension methods are determined at compile-time (early bound).

Note that the guideline applies to other languages where the same binding behavior is present, or 
where extension methods are not supported.

:dont:`DO NOT` put extension methods in the same namespace as the extended type unless it is for adding 
methods to interfaces or for dependency management.

:avoid:`AVOID` defining two or more extension methods with the same signature, even if they reside in 
different namespaces.

:consider:`CONSIDER` defining extension methods in the same namespace as the extended type if the type is an 
interface and if the extension methods are meant to be used in most or all cases.

:dont:`DO NOT` define extension methods implementing a feature in namespaces normally associated with other 
features. Instead, define them in the namespace associated with the feature they belong to.

:avoid:`AVOID` generic naming of namespaces dedicated to extension methods (e.g., "Extensions"). Use a 
descriptive name (e.g., "Routing") instead.

Operator overloads
------------------

Operator overloads allow framework types to appear as if they were built-in language primitives.

Although allowed and useful in some situations, operator overloads should be used cautiously. There 
are many cases in which operator overloading has been abused, such as when framework designers started 
to use operators for operations that should be simple methods. The following guidelines should help you 
decide when and how to use operator overloading.

:avoid:`AVOID` defining operator overloads, except in types that should feel like primitive (built-in) types.

:consider:`CONSIDER` defining operator overloads in a type that should feel like a primitive type.

For example, System.String has operator== and operator!= defined.

:do:`DO` define operator overloads in structs that represent numbers (such as System.Decimal).

:dont:`DO NOT` be cute when defining operator overloads.

Operator overloading is useful in cases in which it is immediately obvious what the result of the 
operation will be. For example, it makes sense to be able to subtract one DateTime from another DateTime 
and get a TimeSpan. However, it is not appropriate to use the logical union operator to union two 
database queries, or to use the shift operator to write to a stream.

:dont:`DO NOT` provide operator overloads unless at least one of the operands is of the type defining the 
overload.

:do:`DO` overload operators in a symmetric fashion.

For example, if you overload the operator==, you should also overload the operator!=. Similarly, if 
you overload the operator<, you should also overload the operator>, and so on.

:consider:`CONSIDER` providing methods with friendly names that correspond to each overloaded operator.

Many languages do not support operator overloading. For this reason, it is recommended that types that 
overload operators include a secondary method with an appropriate domain-specific name that provides 
equivalent functionality.

The following table contains a list of operators and the corresponding friendly method names.

+-----------------------+-------------------------------+-------------------------------+
| C# Operator Symbol    | Metadata Name                 | Friendly Name                 |
+-----------------------+-------------------------------+-------------------------------+
| N/A                   | op_Implicit                   | To<TypeName>/From<TypeName>   |
+-----------------------+-------------------------------+-------------------------------+
| N/A                   | op_Explicit                   | To<TypeName>/From<TypeName>   |
+-----------------------+-------------------------------+-------------------------------+
| \+ (binary)           | op_Addition                   | Add                           |
+-----------------------+-------------------------------+-------------------------------+
| \- (binary)           | op_Subtraction                | Subtract                      |
+-----------------------+-------------------------------+-------------------------------+
| \* (binary)           | op_Multiply                   | Multiply                      |
+-----------------------+-------------------------------+-------------------------------+
| /                     | op_Division                   | Divide                        |
+-----------------------+-------------------------------+-------------------------------+
| %                     | op_Modulus                    | Mod or Remainder              |
+-----------------------+-------------------------------+-------------------------------+
| ^                     | op_ExclusiveOr                | Xor                           |
+-----------------------+-------------------------------+-------------------------------+
| & (binary)            | op_BitwiseAnd                 | BitwiseAnd                    |
+-----------------------+-------------------------------+-------------------------------+
| \|                    | op_BitwiseOr                  | BitwiseOr                     |
+-----------------------+-------------------------------+-------------------------------+
| &&                    | op_LogicalAnd                 | And                           |
+-----------------------+-------------------------------+-------------------------------+
| ||                    | op_LogicalOr                  | Or                            |
+-----------------------+-------------------------------+-------------------------------+
| =                     | op_Assign                     | Assign                        |
+-----------------------+-------------------------------+-------------------------------+
| <<                    | op_LeftShift                  | LeftShift                     |
+-----------------------+-------------------------------+-------------------------------+
| >>                    | op_RightShift                 | RightShift                    |
+-----------------------+-------------------------------+-------------------------------+
| N/A                   | op_SignedRightShift           | SignedRightShift              |
+-----------------------+-------------------------------+-------------------------------+
| N/A                   | op_UnsignedRightShift         | UnsignedRightShift            |
+-----------------------+-------------------------------+-------------------------------+
| ==                    | op_Equality                   | Equals                        |
+-----------------------+-------------------------------+-------------------------------+
| !=                    | op_Inequality                 | Equals                        |
+-----------------------+-------------------------------+-------------------------------+
| >                     | op_GreaterThan                | CompareTo                     |
+-----------------------+-------------------------------+-------------------------------+
| <                     | op_LessThan                   | CompareTo                     |
+-----------------------+-------------------------------+-------------------------------+
| >=                    | op_GreaterThanOrEqual         | CompareTo                     |
+-----------------------+-------------------------------+-------------------------------+
| <=                    | op_LessThanOrEqual            | CompareTo                     |
+-----------------------+-------------------------------+-------------------------------+
| *=                    | op_MultiplicationAssignment   | Multiply                      |
+-----------------------+-------------------------------+-------------------------------+
| -=                    | op_SubtractionAssignment      | Subtract                      |
+-----------------------+-------------------------------+-------------------------------+
| ^=                    | op_ExclusiveOrAssignment      | Xor                           |
+-----------------------+-------------------------------+-------------------------------+
| <<=                   | op_LeftShiftAssignment        | LeftShift                     |
+-----------------------+-------------------------------+-------------------------------+
| >>=                   | op_RightShiftAssignment       | RightShift                    |
+-----------------------+-------------------------------+-------------------------------+
| %=                    | op_ModulusAssignment          | Mod                           |
+-----------------------+-------------------------------+-------------------------------+
| +=                    | op_AdditionAssignment         | Add                           |
+-----------------------+-------------------------------+-------------------------------+
| &=                    | op_BitwiseAndAssignment       | BitwiseAnd                    |
+-----------------------+-------------------------------+-------------------------------+
| |=                    | op_BitwiseOrAssignment        | BitwiseOr                     |
+-----------------------+-------------------------------+-------------------------------+
| ,                     | op_Comma                      | Comma                         |
+-----------------------+-------------------------------+-------------------------------+
| /=                    | op_DivisionAssignment         | Divide                        |
+-----------------------+-------------------------------+-------------------------------+
| \- \-                 | op_Decrement                  | Decrement                     |
+-----------------------+-------------------------------+-------------------------------+
| ++                    | op_Increment                  | Increment                     |
+-----------------------+-------------------------------+-------------------------------+
| \- (unary)            | op_UnaryNegation              | Negate                        |
+-----------------------+-------------------------------+-------------------------------+
| \+ (unary)            | op_UnaryPlus                  | Plus                          |
+-----------------------+-------------------------------+-------------------------------+
| ~                     | op_OnesComplement             | OnesComplement                |
+-----------------------+-------------------------------+-------------------------------+

Overloading Operator ==
^^^^^^^^^^^^^^^^^^^^^^^

Overloading operator == is quite complicated. The semantics of the operator need to be compatible with 
several other members, such as Object.Equals.

Conversion Operators
^^^^^^^^^^^^^^^^^^^^

Conversion operators are unary operators that allow conversion from one type to another. The operators 
must be defined as static members on either the operand or the return type. There are two types of 
conversion operators: implicit and explicit.

:dont:`DO NOT` provide a conversion operator if such conversion is not clearly expected by the end users.

:dont:`DO NOT` define conversion operators outside of a type’s domain.

For example, Int32, Double, and Decimal are all numeric types, whereas DateTime is not. Therefore, 
there should be no conversion operator to convert a Double(long) to a DateTime. A constructor is 
preferred in such a case.

:dont:`DO NOT` provide an implicit conversion operator if the conversion is potentially lossy.

For example, there should not be an implicit conversion from Double to Int32 because Double has a 
wider range than Int32. An explicit conversion operator can be provided even if the conversion is 
potentially lossy.

:dont:`DO NOT` throw exceptions from implicit casts.

It is very difficult for end users to understand what is happening, because they might not be aware 
that a conversion is taking place.

:do:`DO` throw System.InvalidCastException if a call to a cast operator results in a lossy conversion and 
the contract of the operator does not allow lossy conversions.

Parameter Design
----------------

This section provides broad guidelines on parameter design, including sections with guidelines for 
checking arguments. In addition, you should refer to the guidelines described in Naming Parameters.

:do:`DO` use the least derived parameter type that provides the functionality required by the member.

For example, suppose you want to design a method that enumerates a collection and prints each item to 
the console. Such a method should take IEnumerable as the parameter, not ArrayList or IList, for 
example.

:dont:`DO NOT` use reserved parameters.

If more input to a member is needed in some future version, a new overload can be added.

:dont:`DO NOT` have publicly exposed methods that take pointers, arrays of pointers, or multidimensional 
arrays as parameters.

Pointers and multidimensional arrays are relatively difficult to use properly. In almost all cases, 
APIs can be redesigned to avoid taking these types as parameters.

:do:`DO` place all out parameters following all of the by-value and ref parameters (excluding parameter 
arrays), even if it results in an inconsistency in parameter ordering between overloads (see Member 
Overloading).

The out parameters can be seen as extra return values, and grouping them together makes the method 
signature easier to understand.

:do:`DO` be consistent in naming parameters when overriding members or implementing interface members.

This better communicates the relationship between the methods.

Choosing Between Enum and Boolean Parameters
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:do:`DO` use enums if a member would otherwise have two or more Boolean parameters.

:dont:`DO NOT` use Booleans unless you are absolutely sure there will never be a need for more than two 
values.

Enums give you some room for future addition of values, but you should be aware of all the implications 
of adding values to enums, which are described in Enum Design.

:consider:`CONSIDER` using Booleans for constructor parameters that are truly two-state values and are simply 
used to initialize Boolean properties.

Validating Arguments
^^^^^^^^^^^^^^^^^^^^

:do:`DO` validate arguments passed to public, protected, or explicitly implemented members. Throw 
System.ArgumentException, or one of its subclasses, if the validation fails.

Note that the actual validation does not necessarily have to happen in the public or protected member 
itself. It could happen at a lower level in some private or internal routine. The main point is that 
the entire surface area that is exposed to the end users checks the arguments.

:do:`DO` throw ArgumentNullException if a null argument is passed and the member does not support null 
arguments.

:do:`DO` validate enum parameters.

Do not assume enum arguments will be in the range defined by the enum. The CLR allows casting any 
integer value into an enum value even if the value is not defined in the enum.

:dont:`DO NOT` use Enum.IsDefined for enum range checks.

:do:`DO` be aware that mutable arguments might have changed after they were validated.

If the member is security sensitive, you are encouraged to make a copy and then validate and process 
the argument.

Parameter Passing
^^^^^^^^^^^^^^^^^

From the perspective of a framework designer, there are three main groups of parameters: by-value 
parameters, ref parameters, and out parameters.

When an argument is passed through a by-value parameter, the member receives a copy of the actual 
argument passed in. If the argument is a value type, a copy of the argument is put on the stack. 
If the argument is a reference type, a copy of the reference is put on the stack. Most popular CLR 
languages, such as C#, VB.NET, and C++, default to passing parameters by value.

When an argument is passed through a ref parameter, the member receives a reference to the actual 
argument passed in. If the argument is a value type, a reference to the argument is put on the stack. 
If the argument is a reference type, a reference to the reference is put on the stack. Ref parameters 
can be used to allow the member to modify arguments passed by the caller.

Out parameters are similar to ref parameters, with some small differences. The parameter is initially 
considered unassigned and cannot be read in the member body before it is assigned some value. Also, 
the parameter has to be assigned some value before the member returns.

:avoid:`AVOID` using out or ref parameters.

Using out or ref parameters requires experience with pointers, understanding how value types and 
reference types differ, and handling methods with multiple return values. Also, the difference 
between out and ref parameters is not widely understood. Framework architects designing for a general 
audience should not expect users to master working with out or ref parameters.

:dont:`DO NOT` pass reference types by reference.

There are some limited exceptions to the rule, such as a method that can be used to swap references.

Members with Variable Number of Parameters
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Members that can take a variable number of arguments are expressed by providing an array parameter. 
For example, String provides the following method::

    public class String {
        public static string Format(string format, object[] parameters);
    }

A user can then call the String.Format method, as follows::

    String.Format("File {0} not found in {1}",new object[]{filename,directory});

Adding the C# params keyword to an array parameter changes the parameter to a so-called params array 
parameter and provides a shortcut to creating a temporary array::

    public class String {
        public static string Format(string format, params object[] parameters);
    }

Doing this allows the user to call the method by passing the array elements directly in the argument 
list::

    String.Format("File {0} not found in {1}",filename,directory);

Note that the params keyword can be added only to the last parameter in the parameter list.

:consider:`CONSIDER` adding the params keyword to array parameters if you expect the end users to pass arrays 
with a small number of elements. If it’s expected that lots of elements will be passed in common 
scenarios, users will probably not pass these elements inline anyway, and so the params keyword is 
not necessary.

:avoid:`AVOID` using params arrays if the caller would almost always have the input already in an array.

For example, members with byte array parameters would almost never be called by passing individual 
bytes. For this reason, byte array parameters in the .NET Framework do not use the params keyword.

:dont:`DO NOT` use params arrays if the array is modified by the member taking the params array parameter.

Because of the fact that many compilers turn the arguments to the member into a temporary array at the 
call site, the array might be a temporary object, and therefore any modifications to the array will 
be lost.

:consider:`CONSIDER` using the params keyword in a simple overload, even if a more complex overload could not 
use it.

Ask yourself if users would value having the params array in one overload even if it wasn’t in all 
overloads.

:do:`DO` try to order parameters to make it possible to use the params keyword.

:consider:`CONSIDER` providing special overloads and code paths for calls with a small number of arguments in 
extremely performance-sensitive APIs.

This makes it possible to avoid creating array objects when the API is called with a small number of 
arguments. Form the names of the parameters by taking a singular form of the array parameter and 
adding a numeric suffix.

You should only do this if you are going to special-case the entire code path, not just create an 
array and call the more general method.

:do:`DO` be aware that null could be passed as a params array argument.

You should validate that the array is not null before processing.

:dont:`DO NOT` use the varargs methods, otherwise known as the ellipsis.

Some CLR languages, such as C++, support an alternative convention for passing variable parameter 
lists called varargs methods. The convention should not be used in frameworks, because it is not CLS 
compliant.

Pointer Parameters
^^^^^^^^^^^^^^^^^^

In general, pointers should not appear in the public surface area of a well-designed managed code 
framework. Most of the time, pointers should be encapsulated. However, in some cases pointers are 
required for interoperability reasons, and using pointers in such cases is appropriate.

:do:`DO` provide an alternative for any member that takes a pointer argument, because pointers are not 
CLS-compliant.

:avoid:`AVOID` doing expensive argument checking of pointer arguments.

:do:`DO` follow common pointer-related conventions when designing members with pointers.

For example, there is no need to pass the start index, because simple pointer arithmetic can be used 
to accomplish the same result.